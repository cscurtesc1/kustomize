# kustomize

debug kustomize configMap merge feature
```
├── base
│   ├── deployments
│   │   └── web
│   │       ├── deployment.yaml
│   │       └── kustomization.yaml
│   ├── kustomization.yaml
│   └── services
│       ├── kustomization.yaml
│       └── web
│           ├── kustomization.yaml
│           └── service.yaml
├── overlays
│   ├── prod
│   │   ├── kustomization.yaml
│   │   ├── stripe-prod.yaml
│   │   ├── web-patch.yaml
│   │   └── web-prod.conf
│   ├── staging
│   └── test
└── README.md

```

